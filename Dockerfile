FROM node:13-alpine3.10 as builder
WORKDIR /var/app
ADD . .
RUN yarn install --frozen-lockfile
RUN yarn tsc

FROM node:13-alpine3.10 as proddeps
ENV NODE_ENV=production
WORKDIR /var/app
ADD . .
RUN yarn install --frozen-lockfile

FROM node:13-alpine3.10
WORKDIR /var/app
ENV NODE_ENV=production \
    GOOGLE_APPLICATION_CREDENTIALS="./gauth.json"
EXPOSE 3000

ADD gauth.json .

COPY --from=builder /var/app/build build
COPY --from=proddeps /var/app/node_modules node_modules

CMD node ./build/index.js
