import { Express, NextFunction, Request, Response } from 'express';
import expressWinston from 'express-winston';
import bodyParser from 'body-parser';
import { v4 as uuid } from 'uuid';
import cors from 'cors';
import { InvalidActionError, AuthError, InvalidDataError, NotImplementedError } from './errors';
import logger, { transports, format } from './logger';

const IDHEADER = 'x-requestid';

const addRequestid = () => (_request: Request, response: Response, next: NextFunction) => {
  response.setHeader(IDHEADER, uuid());
  next();
};

export const checkForTokenMiddleware = () => (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  if (request.headers.authorization) {
    next();
  } else {
    logger.error('authorization header missing');
    response.status(400);
    response.end('Unauthorized');
  }
};

export const serveOrError = async (fn: () => Promise<void>, response: Response): Promise<void> => {
  try {
    await fn();
  } catch (e) {
    if (e instanceof InvalidActionError) {
      logger.error(e.message, {
        reqid: response.getHeader(IDHEADER),
        stack: e.stack,
      });
      response.status(403);
      response.json({});
      return;
    }
    if (e instanceof AuthError) {
      logger.error(e.message, {
        reqid: response.getHeader(IDHEADER),
        stack: e.stack,
      });
      response.status(401);
      response.json({});
      return;
    }
    if (e instanceof InvalidDataError) {
      logger.error(e.message, {
        reqid: response.getHeader(IDHEADER),
        stack: e.stack,
      });
      response.status(400);
      response.json({});
      return;
    }
    if (e instanceof NotImplementedError) {
      logger.error(e.message, {
        reqid: response.getHeader(IDHEADER),
        stack: e.stack,
      });
      response.status(501);
      response.end('not implemented');
    }
    logger.error(e.message, {
      reqid: response.getHeader(IDHEADER),
      stack: e.stack,
    });
    response.status(500);
    response.json({});
  }
};

const setupMiddleware = (app: Express) => {
  app.use(addRequestid());
  app.use(
    expressWinston.logger({
      transports,
      format,
      colorize: false,
      headerBlacklist: ['user-agent', 'host', 'authorization', 'connection'],
      dynamicMeta: (_, res) => ({
        reqid: res.getHeader(IDHEADER),
      }),
    }),
  );
  app.use(cors());
  app.use(bodyParser.json());
};

export default setupMiddleware;
