import { User } from '@helpinghands/types/src/user';
import { DocumentID } from '@helpinghands/types';
import { admin } from './db/firestore';
import { get_user } from './db/user';
import config from './config';
import logger from './logger';

const { send, debug } = config.pushNotifications;

export const createMessage = (
  token: string,
  title: string,
  body: string,
): admin.messaging.Message => ({
  notification: {
    title,
    body,
  },
  token,
});
export const sendMessage = async (message: admin.messaging.Message) => {
  if (debug) {
    logger.info('sendMessage called', {
      ...message.notification,
    });
  }
  if (!send) {
    return '';
  }
  try {
    return await admin.messaging().send(message);
  } catch (e) {
    logger.error(e);
    return '';
  }
};

export const sendMessageToUser = async (user: User, title: string, body: string) => {
  if (debug) {
    logger.info('sendMessageToUser called', {
      title,
      body,
    });
  }
  if (!send) {
    return '';
  }
  if (user.notificationToken) {
    const message = createMessage(user.notificationToken, title, body);
    return sendMessage(message);
  }
  return '';
};

export const sendMessageToUserID = async (id: DocumentID, title: string, body: string) => {
  if (debug) {
    logger.info('sendMessageToUserId called', {
      title,
      body,
    });
  }
  if (!send) {
    return '';
  }
  const user = await get_user(id);
  return sendMessageToUser(user, title, body);
};
