import winston from 'winston';
import environment from './environment';

export const transports = [new winston.transports.Console()];
const devFormat = winston.format.combine(
  winston.format.timestamp(),
  winston.format.json(),
  winston.format.prettyPrint(),
);
const prodFormat = winston.format.combine(winston.format.json());
export const format = environment === 'development' ? devFormat : prodFormat;

const logger = winston.createLogger({
  level: 'info',
  transports,
  format,
});

export default logger;
