export default class MessageSendError extends Error {
  constructor(message: string = 'Message send error') {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
