export default class InvalidActionError extends Error {
  constructor(message: string = 'Invalid action') {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
