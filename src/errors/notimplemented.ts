export default class NotImplementedError extends Error {
  constructor(message: string = 'Not implemented') {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
