export default class Autherror extends Error {
  constructor(message: string = 'Authentication failed') {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
