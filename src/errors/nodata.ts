export default class NoDataError extends Error {
  constructor(message: string = 'No data found') {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
