export default class InvalidDataError extends Error {
  constructor(message: string = 'Invalid data') {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
