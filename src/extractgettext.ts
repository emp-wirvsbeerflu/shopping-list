// eslint-disable-next-line import/no-extraneous-dependencies
import { GettextExtractor, JsExtractors } from 'gettext-extractor';
import { resolve } from 'path';

const extractor = new GettextExtractor();

const pwd = process.env.PWD as string;

extractor
  .createJsParser([
    JsExtractors.callExpression('gt.gettext', {
      arguments: {
        text: 0,
      },
    }),
  ])
  .parseFilesGlob('./**/*.ts');

const out = resolve(pwd, 'messages/messages.pot');

console.log('out', out);

extractor.savePotFile(out);

extractor.printStats();
