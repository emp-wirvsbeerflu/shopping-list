import {
  User,
  USER_API_VERSION,
  ShoppingListState,
  ShoppingList,
  Document,
  UserDisplayInformation,
} from '@helpinghands/types';
import logger from './logger';

export type GroupedShopplingLists = {
  [key in ShoppingListState]: (Document<Omit<ShoppingList, 'takenBy'>> & {
    /**
     * Hero Photo and name
     */
    takenBy?: UserDisplayInformation;
  })[];
};

export type UserWithGroupedLists = User & GroupedShopplingLists;

export const migrateUser = (user: any): User => {
  if (user.apiVersion === USER_API_VERSION) {
    return user as User;
  }
  logger.info(`migrating user ${user} to ${USER_API_VERSION}`);
  if (typeof user.apiVersion === 'undefined') {
    const newUser = {
      ...user,
      apiVersion: 0,
    };
    return migrateUser(newUser);
  }
  switch (user.apiVersion) {
    case 0: {
      const newUser = {
        ...user,
      };
      if (newUser.displayname) {
        newUser.displayName = user.displayname;
        delete newUser.displayname;
      }
      newUser.apiVersion += 1;
      return migrateUser(newUser);
    }
    default:
      return user as User;
  }
};

export default migrateUser;
