import Gettext from 'node-gettext';
// import { po } from 'gettext-parser';
// import { readFileSync } from 'fs';
// import { resolve } from 'path';

// const pwd = process.env.PWD as string;
// const defaultMessages = readFileSync(resolve(pwd, 'messages/default.pot'));

const gt = new Gettext();

const domain = 'messages';
const locale = 'de';

// gt.addTranslations(locale, domain, po.parse(defaultMessages));

gt.setLocale(locale);
gt.setTextDomain(domain);

export default gt;
