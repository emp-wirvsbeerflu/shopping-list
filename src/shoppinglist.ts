import type { ShoppingList } from '@helpinghands/types';

export const migrateShoppinglist = (list: any): ShoppingList => list as ShoppingList;

export default migrateShoppinglist;
