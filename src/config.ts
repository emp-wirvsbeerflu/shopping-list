import { readFileSync } from 'fs';
import { resolve } from 'path';
import yaml from 'yaml';
import environment from './environment';
import deepmerge from './deepmerge';
import logger from './logger';

const pwd = process.env.PWD as string;

const file = environment ? `${environment}.yaml` : 'development.yaml';

const defaultConfigFile = resolve(pwd, 'config/default.yaml');
const configFile = resolve(pwd, 'config/', file);

logger.info(`parsing config file ${configFile}`);

export type Config = {
  pushNotifications: {
    send: boolean;
    debug: boolean;
  };
  tables: {
    lists: string;
    users: string;
  };
  cache: {
    enabled: boolean;
    debug: boolean;
    cachetime: number;
  };
  blockedIds: string[];
};

const parsedBase = yaml.parse(readFileSync(defaultConfigFile, 'utf8')) as Config;
const parsedEnv = yaml.parse(readFileSync(configFile, 'utf-8')) as Config;

export default deepmerge(parsedBase, parsedEnv);
