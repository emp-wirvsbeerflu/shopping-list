/**
 * <p>Represents a point on the surface of a sphere. (The Earth is almost
 * spherical.)</p>
 *
 * <p>To create an instance, call one of the static methods fromDegrees() or
 * fromRadians().</p>
 *
 * <p>This code was originally published at
 * <a href="http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates#Java">
 * http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates#Java</a>.</p>
 *
 * @author Jan Philip Matuschek
 * @version 22 September 2010
 */

interface GeoLocation {
  radiansLatitude: number; // latitude in radians
  radiansLongitude: number; // longitude in radians
  degreeLatitude: number; // latitude in degrees
  degreeLongitude: number; //
  checkBounds(): void;
  toString(): string;
  distanceTo(location: GeoLocation, radius: number): number;
  boundingCoordinates(distance: number, radius: number): GeoLocation[];
}

const toRadians = (degree: number): number => degree * (Math.PI / 180);
const toDegrees = (radian: number): number => radian * (180 / Math.PI);

const MIN_LAT = toRadians(-90); // -PI/2
const MAX_LAT = toRadians(90); //  PI/2
const MIN_LON = toRadians(-180); // -PI
const MAX_LON = toRadians(180); //  PI

const RADIUS_EARTH = 6371.01;

class GeoLocation implements GeoLocation {
  public radiansLatitude = 0; // latitude in radians

  public radiansLongitude = 0; // longitude in radians

  public degreeLatitude = 0; // latitude in degrees

  public degreeLongitude = 0; // longitude in degrees

  constructor(
    radiansLatitude: number,
    radiansLongitude: number,
    degreeLatitude: number,
    degreeLongitude: number,
  ) {
    this.radiansLatitude = radiansLatitude;
    this.radiansLongitude = radiansLongitude;
    this.degreeLatitude = degreeLatitude;
    this.degreeLongitude = degreeLongitude;
  }

  public checkBounds() {
    if (
      this.radiansLatitude < MIN_LAT ||
      this.radiansLatitude > MAX_LAT ||
      this.radiansLongitude < MIN_LON ||
      this.radiansLongitude > MAX_LON
    )
      throw new Error('Illegal Argument');
  }

  public toString() {
    return `(${this.degreeLatitude}°, ${this.degreeLongitude}°) = (${this.radiansLatitude} rad, ${this.radiansLongitude} rad)`;
  }

  /**
   * Computes the great circle distance between this GeoLocation instance
   * and the location argument.
   * @param location the GeoLocation to get the distance from
   * @param radius the radius of the sphere, e.g. the average radius for a
   * spherical approximation of the figure of the Earth is approximately
   * 6371.01 kilometers.
   * @return the distance, measured in the same unit as the radius
   * argument.
   */
  public distanceTo(location: GeoLocation, radius: number = RADIUS_EARTH) {
    return (
      Math.acos(
        Math.sin(this.radiansLatitude) * Math.sin(location.radiansLatitude) +
          Math.cos(this.radiansLatitude) *
            Math.cos(location.radiansLatitude) *
            Math.cos(this.radiansLongitude - location.radiansLongitude),
      ) * radius
    );
  }

  /**
   * <p>Computes the bounding coordinates of all points on the surface
   * of a sphere that have a great circle distance to the point represented
   * by this GeoLocation instance that is less or equal to the distance
   * argument.</p>
   * <p>For more information about the formulae used in this method visit
   * <a href="http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates">
   * http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates</a>.</p>
   * @param distance the distance from the point represented by this
   * GeoLocation instance. Must me measured in the same unit as the radius
   * argument.
   * @param radius the radius of the sphere, e.g. the average radius for a
   * spherical approximation of the figure of the Earth is approximately
   * 6371.01 kilometers.
   * @return an array of two GeoLocation objects such that:<ul>
   * <li>The latitude of any point within the specified distance is greater
   * or equal to the latitude of the first array element and smaller or
   * equal to the latitude of the second array element.</li>
   * <li>If the longitude of the first array element is smaller or equal to
   * the longitude of the second element, then
   * the longitude of any point within the specified distance is greater
   * or equal to the longitude of the first array element and smaller or
   * equal to the longitude of the second array element.</li>
   * <li>If the longitude of the first array element is greater than the
   * longitude of the second element (this is the case if the 180th
   * meridian is within the distance), then
   * the longitude of any point within the specified distance is greater
   * or equal to the longitude of the first array element
   * <strong>or</strong> smaller or equal to the longitude of the second
   * array element.</li>
   * </ul>
   */
  public boundingCoordinates(distance: number, radius: number = RADIUS_EARTH) {
    if (radius < 0 || distance < 0) throw new Error('illegal argument');

    // angular distance in radians on a great circle
    const radDist = distance / radius;

    let minLat = this.radiansLatitude - radDist;
    let maxLat = this.radiansLatitude + radDist;

    let minLon;
    let maxLon;
    if (minLat > MIN_LAT && maxLat < MAX_LAT) {
      const deltaLon = Math.asin(Math.sin(radDist) / Math.cos(this.radiansLatitude));
      minLon = this.radiansLongitude - deltaLon;
      if (minLon < MIN_LON) minLon += 2 * Math.PI;
      maxLon = this.radiansLongitude + deltaLon;
      if (maxLon > MAX_LON) maxLon -= 2 * Math.PI;
    } else {
      // a pole is within the distance
      minLat = Math.max(minLat, MIN_LAT);
      maxLat = Math.min(maxLat, MAX_LAT);
      minLon = MIN_LON;
      maxLon = MAX_LON;
    }
    /* eslint-disable  @typescript-eslint/no-use-before-define */
    return [
      createGeoLocationFromRadians(minLat, minLon),
      createGeoLocationFromRadians(
        maxLat,
        maxLon,
      ) /* eslint-enable  @typescript-eslint/no-use-before-define */,
    ];
  }
}

/**
 * @param latitude the latitude, in degrees.
 * @param longitude the longitude, in degrees.
 */
export const createGeoLocationFromDegrees = (latitude: number, longitude: number): GeoLocation => {
  const geoLocation = new GeoLocation(
    toRadians(latitude),
    toRadians(longitude),
    latitude,
    longitude,
  );
  geoLocation.checkBounds();
  return geoLocation;
};

/**
 * @param latitude the latitude, in radians.
 * @param longitude the longitude, in radians.
 */
export const createGeoLocationFromRadians = (latitude: number, longitude: number): GeoLocation => {
  const geoLocation = new GeoLocation(
    latitude,
    longitude,
    toDegrees(latitude),
    toDegrees(longitude),
  );
  geoLocation.checkBounds();
  return geoLocation;
};
