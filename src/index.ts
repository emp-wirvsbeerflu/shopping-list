import express from 'express';
import { ShoppingListType } from '@helpinghands/types';
import setupMiddleware from './middleware';
import setupUserRoutes from './routes/user';
import setupHomieRoutes from './routes/homie';
import setupHeroRoutes from './routes/hero';
import setupShoppinglistRoutes from './routes/shoppinglist';
import logger from './logger';

const app = express();

const PORT = process.env.PORT || 3000;

setupMiddleware(app);

setupUserRoutes(app);

setupHomieRoutes(app);

setupHeroRoutes(app);

setupShoppinglistRoutes(app);

app.get('/listtypes', (_request, response) => {
  response.json(Object.keys(ShoppingListType));
});

app.listen(PORT, () => {
  logger.info(`app listening on port ${PORT}`);
});
