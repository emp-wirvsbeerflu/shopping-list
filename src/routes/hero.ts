import { Express } from 'express';
import ShoppingListApi, { UserType } from '@helpinghands/types';
import { checkForTokenMiddleware, serveOrError } from '../middleware';
import { get_lists_taken_by_user, get_user_for_shoppinglists } from '../db/shoppinglist';
import { InvalidActionError } from '../errors';
import { get_current_user_id } from '../db/util';
import { get_user } from '../db/user';

const setupHeroRoutes = (app: Express) => {
  app.get<{}, ShoppingListApi['/hero/lists']['GET']['return']>(
    '/hero/lists',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const tokenid = await get_current_user_id(request.headers.authorization as string);
        const user = await get_user(tokenid);
        if (user.type !== UserType.HERO) {
          throw new InvalidActionError('User is not a HERO');
        }

        const lists = await get_lists_taken_by_user(tokenid);
        const lists_with_users = await get_user_for_shoppinglists(lists);

        response.json(lists_with_users);
      }, response);
    },
  );
};

export default setupHeroRoutes;
