import { Express } from 'express';
import ShoppingListApi, { UserType, DocumentID } from '@helpinghands/types';
import { checkForTokenMiddleware, serveOrError } from '../middleware';
import {
  get_lists_by_user,
  get_takenby_user_for_shoppinglists,
  get_open_lists_sorted_by_distance,
  get_open_lists_by_user,
} from '../db/shoppinglist';
import { InvalidActionError, NotImplementedError } from '../errors';
import { get_current_user_id } from '../db/util';
import { get_user } from '../db/user';

const setupHomieRoutes = (app: Express) => {
  app.get<{}, ShoppingListApi['/homie/lists']['GET']['return']>(
    '/homie/lists',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const tokenid = await get_current_user_id(request.headers.authorization as string);
        const { type } = await get_user(tokenid);
        if (type !== UserType.HOMIE) {
          throw new InvalidActionError(`User ${tokenid} is not a Homie`);
        }
        const lists = await get_lists_by_user(tokenid);

        const listsWithUser = await get_takenby_user_for_shoppinglists(lists);

        response.json(listsWithUser);
      }, response);
    },
  );
  app.get<{}, ShoppingListApi['/homies/:page?']['GET']['return']>(
    '/homies',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const userid = await get_current_user_id(request.headers.authorization as string);
        const result = await get_open_lists_sorted_by_distance(userid);
        response.json(result);
      }, response);
    },
  );

  app.get('/homies/:page', async (request, response) => {
    serveOrError(() => {
      throw new NotImplementedError();
    }, response);
  });

  app.get<{ id: DocumentID }, ShoppingListApi['/homie/:id']['GET']['return']>(
    '/homie/:id',
    checkForTokenMiddleware(),
    async (request, response) => {
      const { id } = request.params;
      serveOrError(async () => {
        const { displayName, photoURL, type } = await get_user(id);
        if (type !== UserType.HOMIE) {
          throw new InvalidActionError(`user ${id} is not a homie`);
        }
        const shoppinglists = await get_open_lists_by_user(id);
        response.json({
          displayName,
          photoURL,
          shoppinglists,
        });
      }, response);
    },
  );
};

export default setupHomieRoutes;
