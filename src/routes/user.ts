import { Express } from 'express';
import ShoppingListApi, { Document, User, SuccessResponse } from '@helpinghands/types';
import { checkForTokenMiddleware, serveOrError } from '../middleware';
import { get_current_user_id } from '../db/util';
import { create_user, update_user, delete_user, get_user_with_open_tasks } from '../db/user';
import { purge_stale_cache_entries } from '../db/userCache';

const setupUserRoutes = (app: Express) => {
  app.get('/user/purgeCache', (request, response) => {
    purge_stale_cache_entries();
    response.json({
      result: 'ok',
    });
  });

  app.get<{}, ShoppingListApi['/user']['GET']['return'], null>(
    '/user',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const tokenid = await get_current_user_id(request.headers.authorization as string);
        const user = await get_user_with_open_tasks(tokenid);
        response.json(user);
      }, response);
    },
  );

  app.post<{}, Document<User>, ShoppingListApi['/user']['POST']['params']>(
    '/user',
    checkForTokenMiddleware(),
    async (request, response) => {
      const reqBody = request.body;
      serveOrError(async () => {
        const tokenid = await get_current_user_id(request.headers.authorization as string);
        const user = await create_user(tokenid, reqBody);
        response.json(user);
      }, response);
    },
  );

  app.put<{}, SuccessResponse, ShoppingListApi['/user']['PUT']['params']>(
    '/user',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const tokenid = await get_current_user_id(request.headers.authorization as string);
        await update_user(tokenid, request.body);
        response.json({
          result: 'ok',
        });
      }, response);
    },
  );

  app.delete<{}, SuccessResponse, {}>(
    '/user',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const tokenid = await get_current_user_id(request.headers.authorization as string);
        delete_user(tokenid);
        response.json({
          result: 'ok',
        });
      }, response);
    },
  );
};

export default setupUserRoutes;
