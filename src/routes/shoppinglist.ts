import { Express } from 'express';
import ShoppingListApi, {
  UserType,
  DocumentID,
  SuccessResponse,
  ShoppingListState,
  Document,
  ShoppingList,
} from '@helpinghands/types';
import { checkForTokenMiddleware, serveOrError } from '../middleware';
import {
  get_list_by_id,
  delete_list,
  create_list,
  take_list,
  get_list_document_by_id,
  get_user_for_shoppinglist,
  update_list,
  unpublish_list,
  publish_list,
  untake_list,
  deliver_list,
  finish_deliver_list,
  close_list,
  update_list_items,
  handle_due_lists,
  deliver_partial_list,
  fix_wrong_lists,
} from '../db/shoppinglist';
import { AuthError, InvalidActionError, InvalidDataError } from '../errors';
import { get_current_user_id } from '../db/util';
import { get_user } from '../db/user';
import logger from '../logger';

const setupShoppinglistRoutes = (app: Express) => {
  app.get<{}, SuccessResponse>('/shoppinglist/fixWrongLists', async (_request, response) => {
    serveOrError(async () => {
      await fix_wrong_lists();
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.get<{}, SuccessResponse>('/shoppinglist/checkDueDate', async (_request, response) => {
    serveOrError(async () => {
      const now = Date.now();
      await handle_due_lists(now);
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.delete<
    {
      id: DocumentID;
    },
    SuccessResponse,
    {}
  >('/shoppinglist/:id', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const tokenid = await get_current_user_id(request.headers.authorization as string);
      const list = await get_list_by_id(id);

      if (tokenid !== list.user) {
        throw new AuthError('Tried to delete a foreign list!');
      }

      if (
        list.state !== ShoppingListState.IN_CREATION &&
        list.state !== ShoppingListState.PUBLISHED
      ) {
        throw new InvalidActionError(`The list is ${list.state}, can't delete that!`);
      }

      await delete_list(id);

      response.json({ result: 'ok' });
    }, response);
  });

  app.post<{}, Document<ShoppingList>, ShoppingListApi['/shoppinglist']['POST']['params']>(
    '/shoppinglist',
    checkForTokenMiddleware(),
    async (request, response) => {
      serveOrError(async () => {
        const userid = await get_current_user_id(request.headers.authorization as string);
        const user = await get_user(userid);
        if (user.type !== 'HOMIE') {
          throw new InvalidActionError("Heroes can't create lists");
        }
        const list = await create_list(userid, request.body);
        response.json(list);
      }, response);
    },
  );

  // This needs to come before any /shoppinglist/:id route or this will never match
  app.post<
    {},
    ShoppingListApi['/shoppinglist/take']['POST']['return'],
    ShoppingListApi['/shoppinglist/take']['POST']['params']
  >('/shoppinglist/take', checkForTokenMiddleware(), async (request, response) => {
    const { shoppinglists: ids } = request.body;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      const user = await get_user(userid);
      if (user.type !== 'HERO') {
        throw new InvalidDataError('user is not a HERO');
      }
      await Promise.all(
        ids.map(async (id) => {
          // Just move on in case on of the lists goes wrong
          try {
            await take_list(userid, id);
          } catch (e) {
            logger.error(`${e.message}, ${e.stack}`);
            console.error(e);
          }
        }),
      );
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.get<{ id: DocumentID }, ShoppingListApi['/shoppinglist/:id']['GET']['return']>(
    '/shoppinglist/:id',
    checkForTokenMiddleware(),
    async (request, response) => {
      const { id } = request.params;
      serveOrError(async () => {
        const list = await get_list_document_by_id(id);
        const result = await get_user_for_shoppinglist(list);
        response.json(result);
      }, response);
    },
  );

  app.put<
    { id: DocumentID },
    Document<ShoppingList>,
    ShoppingListApi['/shoppinglist/:id']['PUT']['params']
  >('/shoppinglist/:id', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      const currentList = await get_list_by_id(id);
      if (currentList.user !== userid) {
        throw new InvalidActionError('user tried to update a foreign list');
      }
      const list = await update_list(id, request.body);
      response.json(list);
    }, response);
  });

  app.get<{ id: DocumentID }, ShoppingListApi['/shoppinglist/:id/edit']['GET']['return']>(
    '/shoppinglist/:id/edit',
    checkForTokenMiddleware(),
    async (request, response) => {
      const { id } = request.params;
      serveOrError(async () => {
        const userid = await get_current_user_id(request.headers.authorization as string);
        const { type, items } = await get_list_by_id(id);
        await unpublish_list(userid, id);

        response.json({
          type,
          items,
        });
      }, response);
    },
  );

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/publish']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/publish']['POST']['params']
  >('/shoppinglist/:id/publish', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      const list = await get_list_by_id(id);
      if (list.user !== userid) {
        throw new InvalidActionError('user tried to publish a foreign list');
      }
      await publish_list(id, userid);
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/unpublish']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/unpublish']['POST']['params']
  >('/shoppinglist/:id/unpublish', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      await unpublish_list(userid, id);
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/untake']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/untake']['POST']['params']
  >('/shoppinglist/:id/untake', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      const user = await get_user(userid);
      if (user.type !== UserType.HERO) {
        throw new InvalidActionError('user is not a HERO');
      }

      await untake_list(userid, id);

      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/deliver']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/deliver']['POST']['params']
  >('/shoppinglist/:id/deliver', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    const { price, items } = request.body;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      if (items && items.length > 0) {
        await deliver_partial_list(id, userid, price, items);
      } else {
        await deliver_list(id, userid, price);
      }
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/delivered']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/delivered']['POST']['params']
  >('/shoppinglist/:id/delivered', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      await finish_deliver_list(id, userid);
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/close']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/close']['POST']['params']
  >('/shoppinglist/:id/close', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    serveOrError(async () => {
      const userid = await get_current_user_id(request.headers.authorization as string);
      await close_list(id, userid);
      response.json({
        result: 'ok',
      });
    }, response);
  });

  app.post<
    { id: DocumentID },
    ShoppingListApi['/shoppinglist/:id/update-items']['POST']['return'],
    ShoppingListApi['/shoppinglist/:id/update-items']['POST']['params']
  >('/shoppinglist/:id/update-items', checkForTokenMiddleware(), async (request, response) => {
    const { id } = request.params;
    const { items } = request.body;
    serveOrError(async () => {
      await update_list_items(id, items);

      response.json({
        result: 'ok',
      });
    }, response);
  });
};

export default setupShoppinglistRoutes;
