import ShoppingListApi, {
  User,
  DocumentID,
  USER_API_VERSION,
  Document,
  ShoppingList,
  SHOPPINGLIST_API_VERSION,
  UserDisplayInformation,
  ShoppingListState,
  UserType,
} from '@helpinghands/types';
import firestore, { admin } from './firestore';
import { NoDataError, InvalidActionError } from '../errors';
import { try_truncate_radius } from './util';
import { GroupedShopplingLists } from '../user';
import logger from '../logger';
import config from '../config';
import { get_cached_user, purge_user_from_cache, store_user_in_cache } from './userCache';

const {
  tables: { users: usersTable, lists: listsTable },
  blockedIds,
} = config;

export const update_user = async (id: DocumentID, updateData: Partial<User>) => {
  const document = firestore.doc(`${usersTable}/${id}`);
  const snapshot = await document.get();
  const data = snapshot.data();
  if (!data) {
    throw new NoDataError(`user ${id} has no data`);
  }
  purge_user_from_cache(id);
  await document.update({
    ...updateData,
  });
};

export const get_user = async (id: DocumentID): Promise<User> => {
  const cacheEntry = get_cached_user(id);
  if (cacheEntry) {
    return try_truncate_radius(cacheEntry);
  }
  const document = firestore.doc(`${usersTable}/${id}`);
  const snapShot = await document.get();
  const data = snapShot.data() as User | undefined;
  if (!data) {
    throw new NoDataError(`user ${id} has no data`);
  }
  store_user_in_cache(id, data);
  return try_truncate_radius(data);
};

export const get_user_document = async (id: DocumentID): Promise<Document<User>> => {
  const user = await get_user(id);
  return {
    ...user,
    id,
    apiVersion: USER_API_VERSION,
  };
};

export const create_user = async (id: DocumentID, createData: User): Promise<Document<User>> => {
  if (blockedIds.includes(id)) {
    throw new InvalidActionError(`user ${id} is blocked`);
  }
  await firestore.collection(usersTable).doc(id).set(createData);
  return {
    id,
    apiVersion: USER_API_VERSION,
    ...createData,
  } as Document<User>;
};

export const get_all_users = async (): Promise<Document<User>[]> => {
  const snapShot = await firestore.collection(usersTable).get();
  return Promise.all(
    snapShot.docs.map(async (userSnapshot) => {
      const user = try_truncate_radius(userSnapshot.data() as User);
      const { id } = userSnapshot;
      return {
        id,
        apiVersion: USER_API_VERSION,
        ...user,
      } as Document<User>;
    }),
  );
};

export const get_user_with_grouped_lists = async (
  userid: DocumentID,
): Promise<ShoppingListApi['/user']['GET']['return']> => {
  const user = await get_user(userid);
  const listSnapshot = await firestore.collection(listsTable).where('user', '==', userid).get();

  const lists: Document<ShoppingList>[] = listSnapshot.docs.map((document) => {
    const data = document.data() as ShoppingList | undefined;
    if (!data) {
      throw new NoDataError(`list ${document.id} has no data`);
    }
    return {
      id: document.id,
      apiVersion: SHOPPINGLIST_API_VERSION,
      ...data,
    };
  });

  const takenByIdList = lists
    .map(({ takenBy }) => takenBy)
    .filter((takenBy) => typeof takenBy !== 'undefined' && takenBy != null) as string[];

  // using a for loop is way easier than trying to get map/reduce to work with async
  const takenByUserMap: { [key: string]: UserDisplayInformation } = {};

  const userDocumentPromises: Promise<Document<User>>[] = [];

  takenByIdList.forEach((id) => {
    userDocumentPromises.push(get_user_document(id));
  });

  const userDocuments = await Promise.all(userDocumentPromises);

  userDocuments.forEach(({ id, displayName, photoURL }) => {
    takenByUserMap[id] = {
      displayName,
      photoURL,
    };
  });

  const groupedLists: GroupedShopplingLists = {
    IN_CREATION: [],
    PUBLISHED: [],
    TAKEN: [],
    DELIVERED: [],
    ON_DELIVERY: [],
    CLOSED: [],
  };

  let openTasks = 0;

  const filledGroupedLists = listSnapshot.docs.reduce((acc, document) => {
    const data = document.data() as ShoppingList | undefined;
    if (!data) {
      throw new NoDataError(`list ${document.id} has no data`);
    }
    const list: Document<ShoppingList> = {
      id: document.id,
      apiVersion: SHOPPINGLIST_API_VERSION,
      ...data,
    };

    switch (list.state) {
      case ShoppingListState.PUBLISHED:
      case ShoppingListState.TAKEN:
      case ShoppingListState.ON_DELIVERY:
      case ShoppingListState.DELIVERED:
        openTasks += 1;
        break;
      default:
        break;
    }

    // handle this extra or we get lots of `takenBy: null` entries
    if (typeof list.takenBy !== 'undefined') {
      return {
        ...acc,
        [list.state]: [
          ...groupedLists[list.state],
          {
            ...list,
            takenBy: takenByUserMap[list.takenBy],
          },
        ],
      };
    }
    return {
      ...acc,
      [list.state]: [...groupedLists[list.state], list],
    };
  }, groupedLists);

  const userWithGroupedLists = {
    id: userid,
    apiVersion: USER_API_VERSION,
    ...try_truncate_radius(user),
    ...filledGroupedLists,
    openTasks,
  };

  return userWithGroupedLists;
};

export const delete_user = async (id: DocumentID) => {
  logger.info(`DELETE requested for ${id}`);
  // clean up the user
  await firestore.doc(`${usersTable}/${id}`).delete();
  // clean up all lists owned by the user if it is a HOMIE
  const ownedSnapshot = await firestore.collection(listsTable).where('user', '==', id).get();
  if (!ownedSnapshot.empty) {
    const ownedBatch = firestore.batch();
    ownedSnapshot.docs.forEach((doc) => {
      logger.info(`deleting list ${doc.id}`);
      ownedBatch.delete(doc.ref);
    });
    await ownedBatch.commit();
  }
  // free all lists taken by the user if it is a HERO
  const takenSnapshot = await firestore.collection(listsTable).where('takenBy', '==', id).get();
  if (!takenSnapshot.empty) {
    const takenBatch = firestore.batch();
    takenSnapshot.docs.forEach((doc) => {
      const list = doc.data() as ShoppingList | undefined;
      // Don't try to reset lists that are already closed
      if (typeof list === 'undefined') {
        return;
      }
      if (list.state === ShoppingListState.CLOSED) {
        takenBatch.update(doc.ref, {
          takenBy: admin.firestore.FieldValue.delete(),
        });
      } else {
        logger.info(`setting list ${doc.id} back to PUBLISHED`);
        takenBatch.update(doc.ref, {
          state: ShoppingListState.PUBLISHED,
          takenBy: admin.firestore.FieldValue.delete(),
        });
      }
    });
    purge_user_from_cache(id);
    await takenBatch.commit();
  }
};

export const get_user_with_open_tasks = async (
  userid: DocumentID,
): Promise<ShoppingListApi['/user']['GET']['return']> => {
  const user = await get_user(userid);
  let openTasks = 0;
  if (user.type === UserType.HOMIE) {
    const listSnapshot = await firestore.collection(listsTable).where('user', '==', userid).get();

    openTasks = listSnapshot.docs
      .map((document) => {
        const data = document.data() as ShoppingList | undefined;
        if (!data) {
          throw new NoDataError(`list ${document.id} has no data`);
        }
        return data;
      })
      .filter(({ state }) => state !== ShoppingListState.CLOSED).length;
  } else if (user.type === UserType.HERO) {
    const listSnapshot = await firestore.collection(listsTable).where('user', '==', userid).get();

    openTasks = listSnapshot.docs
      .map((document) => {
        const data = document.data() as ShoppingList | undefined;
        if (!data) {
          throw new NoDataError(`list ${document.id} has no data`);
        }
        return data;
      })
      .filter(
        ({ state }) => state === ShoppingListState.TAKEN || state === ShoppingListState.ON_DELIVERY,
      ).length;
  }
  return {
    ...user,
    openTasks,
  };
};
