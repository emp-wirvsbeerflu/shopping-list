import { Firestore } from '@google-cloud/firestore';
import admin from 'firebase-admin';

const firestore = new Firestore();

type Migration = () => Promise<void[]>;

export const needs_help_to_homie: Migration = async () => {
  console.info('Migrating User.type from NEEDS_HELP to HOMIE');
  const querySnapshot = await firestore.collection('users').where('type', '==', 'NEEDS_HELP').get();
  return Promise.all(
    querySnapshot.docs.map(async ({ id }) => {
      console.info('Migrating User#', id);
      const document = firestore.doc(`users/${id}`);
      await document.update({
        type: 'HOMIE',
      });
    }),
  );
};

export const rename_photourl: Migration = async () => {
  console.info('Migrating User.photoUrl to photoURL');
  // This will only select entries where that field exists
  const querySnapshot = await firestore.collection('users').orderBy('photoUrl').get();
  return Promise.all(
    querySnapshot.docs.map(async ({ id }) => {
      console.info('Migrating User#', id);
      const document = firestore.doc(`users/${id}`);
      const snapshot = await document.get();
      const data = snapshot.data();
      if (data) {
        await document.update({
          photoURL: data.photoUrl,
          photoUrl: admin.firestore.FieldValue.delete(),
        });
      }
    }),
  );
};

export const rename_long_to_lng: Migration = async () => {
  console.info('Migrating User.location.long to User.location.lng');
  const querySnapshot = await firestore.collection('users').orderBy('location.long').get();
  return Promise.all(
    querySnapshot.docs.map(async ({ id }) => {
      console.info('Migrating User#', id);
      const document = firestore.doc(`users/${id}`);
      const snapshot = await document.get();
      const data = snapshot.data();
      if (data) {
        const location = {
          lat: data.location.lat,
          lng: data.location.long,
        };
        await document.update({
          location,
        });
      }
    }),
  );
};

export const remove_count_field: Migration = async () => {
  console.info('Migrating Shoppinglist.count to no longer exist');
  const querySnapshot = await firestore.collection('lists').orderBy('count').get();
  return Promise.all(
    querySnapshot.docs.map(async ({ id }) => {
      console.info('Migrating Shoppinglist#', id);
      const document = firestore.doc(`lists/${id}`);
      await document.update({
        count: admin.firestore.FieldValue.delete(),
      });
    }),
  );
};

export const migrate = async () => {
  await needs_help_to_homie();
  await rename_photourl();
  await rename_long_to_lng();
  await remove_count_field();
};

migrate().then(() => {
  console.info('done migrating');
});
