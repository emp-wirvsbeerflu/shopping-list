import { DocumentID, User } from '@helpinghands/types';
import config from '../config';
import logger from '../logger';

const {
  cache: { enabled, debug, cachetime },
} = config;

type UserCache = {
  [key: string]: User & {
    timestamp: number;
  };
};

const userCache: UserCache = {};

export const store_user_in_cache = (id: DocumentID, user: User) => {
  if (enabled) {
    if (debug) {
      logger.info(`user ${id} stored in cache`);
    }
    userCache[id] = {
      ...user,
      timestamp: Date.now(),
    };
  }
};

export const purge_user_from_cache = (id: DocumentID) => {
  if (enabled && userCache[id]) {
    delete userCache[id];
  }
};

export const purge_stale_cache_entries = () => {
  if (!enabled) {
    return;
  }
  const now = Date.now();
  const cutoff = now - cachetime;

  Object.keys(userCache).forEach((id) => {
    const entry = userCache[id];
    if (entry.timestamp < cutoff) {
      purge_user_from_cache(id);
    }
  });
};

export const get_cached_user = (id: DocumentID): User | undefined => {
  if (!enabled) {
    return undefined;
  }

  const user = userCache[id];
  if (!user) {
    return undefined;
  }
  const now = Date.now();
  const cutoff = now - cachetime;

  if (user.timestamp < cutoff) {
    if (debug) {
      logger.info(`user ${id} entry in cache is stale`, {
        now,
        cutoff,
        timestamp: user.timestamp,
      });
    }
    return undefined;
  }
  if (debug) {
    logger.info(`got user ${id} from cache`, {
      now,
      cutoff,
      timestamp: user.timestamp,
    });
  }
  return user;
};
