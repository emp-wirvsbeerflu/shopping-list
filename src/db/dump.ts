// import * as firestoreExportImport from 'firestore-export-import';
// import { resolve } from 'path';
// import { writeFileSync, mkdirSync } from 'fs';
import { User, ShoppingList } from '@helpinghands/types';

// const { FIREBASE_CREDENTIALS_FILE, FIREBASE_DATABASE_URL } = process.env;

// // eslint-disable-next-line import/no-dynamic-require
// const serviceAccount = require(resolve(FIREBASE_CREDENTIALS_FILE as string));

// firestoreExportImport.initializeApp(serviceAccount, FIREBASE_DATABASE_URL as string);

export type DumpedEntries<T> = {
  [id: string]: T;
};

export type Dump = {
  users: DumpedEntries<User>;
  lists: DumpedEntries<ShoppingList>;
};

// firestoreExportImport.backups(['users', 'lists']).then((data: Dump) => {
//   mkdirSync(resolve('./dumps'), { recursive: true });
//   writeFileSync(resolve('./dumps', 'users.json'), JSON.stringify(data.users), {
//     encoding: 'utf-8',
//   });
//   writeFileSync(resolve('./dumps', 'lists.json'), JSON.stringify(data.lists), {
//     encoding: 'utf-8',
//   });
// });
