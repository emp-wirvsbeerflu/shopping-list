import { User, DocumentID } from '@helpinghands/types';
import { admin } from './firestore';
import { AuthError } from '../errors';
import environment from '../environment';

export const truncate_number = (x: number) => Number.parseInt(x.toFixed(1), 10);

export const try_truncate_radius = <T extends User>(user: T): T => {
  if (typeof user !== 'undefined' && typeof user.radius !== 'undefined') {
    const fixed_radius = truncate_number(user.radius);
    return {
      ...user,
      radus: fixed_radius,
    };
  }
  return user;
};

export const get_current_user_id =
  environment === 'development'
    ? async (idtoken: string): Promise<DocumentID> => {
        return idtoken;
      }
    : async (idtoken: string): Promise<DocumentID> => {
        try {
          const decodedToken = await admin.auth().verifyIdToken(idtoken);
          return decodedToken.uid as DocumentID;
        } catch (e) {
          throw new AuthError(e);
        }
      };
