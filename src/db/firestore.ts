import admin from 'firebase-admin';
import { resolve } from 'path';
import { User, ShoppingList } from '@helpinghands/types';
import { DumpedEntries } from './dump';
import logger from '../logger';
import environment from '../environment';
import config from '../config';

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
});

const firestore = admin.firestore();

if (environment === 'development') {
  logger.info(`running in ${environment} mode`);
  firestore.settings({
    projectId: 'test-project-id',
  });
}

export { admin };

export default firestore;

const { FIRESTORE_EMULATOR_HOST, FIREBASE_DATABASE_EMULATOR_HOST } = process.env;

if (FIRESTORE_EMULATOR_HOST && FIREBASE_DATABASE_EMULATOR_HOST && environment === 'development') {
  logger.info('importing mockdata');
  // eslint-disable-next-line import/no-dynamic-require,global-require
  const userDumps: DumpedEntries<User> = require(resolve('./dumps/users.json'));
  Object.keys(userDumps).forEach((id) => {
    logger.info(`inserting user# ${id}`);
    firestore.collection(config.tables.users).doc(id).set(userDumps[id]);
  });
  // eslint-disable-next-line import/no-dynamic-require,global-require
  const listDumps: DumpedEntries<ShoppingList> = require(resolve('./dumps/lists.json'));
  Object.keys(listDumps).forEach((id) => {
    logger.info(`inserting list# ${id}`);
    firestore.collection(config.tables.lists).doc(id).set(listDumps[id]);
  });
}
