import ShoppingListApi, {
  DocumentID,
  Document,
  USER_API_VERSION,
  ShoppingList,
  ShoppingListState,
  SHOPPINGLIST_API_VERSION,
  ShoppingListType,
  ShoppingListBase,
  ShoppingListWithUser,
  UserType,
  User,
} from '@helpinghands/types';
import printf from 'printf';
import { NoDataError, InvalidActionError, InvalidDataError } from '../errors';
import { createGeoLocationFromDegrees as createGeoLocation } from '../geometry/GeoLocation';
import firestore, { admin } from './firestore';
import { try_truncate_radius, truncate_number } from './util';
import { get_user } from './user';
import { sendMessageToUser, sendMessageToUserID } from '../sendmessage';
import gt from '../gettext';
import config from '../config';
import logger from '../logger';

const { lists: listsTable, users: usersTable } = config.tables;

export const get_open_lists_by_user = async (id: DocumentID): Promise<Document<ShoppingList>[]> => {
  const querySnapshot = await firestore
    .collection(listsTable)
    .where('user', '==', id)
    .where('state', '==', ShoppingListState.PUBLISHED)
    .get();
  return querySnapshot.docs.map((document) => {
    const data = document.data() as ShoppingList;
    if (!data) {
      throw new NoDataError(`list ${document.id} has no data`);
    }

    const list: Document<ShoppingList> = {
      id: document.id,
      apiVersion: SHOPPINGLIST_API_VERSION,
      ...data,
    };
    return list;
  });
};

export const get_lists_by_user = async (id: DocumentID): Promise<Document<ShoppingList>[]> => {
  const querySnapshot = await firestore.collection(listsTable).where('user', '==', id).get();
  return querySnapshot.docs.map((document) => {
    const data = document.data() as ShoppingList | undefined;
    if (!data) {
      throw new NoDataError(`list ${document.id} has no data`);
    }
    const list: Document<ShoppingList> = {
      id: document.id,
      apiVersion: SHOPPINGLIST_API_VERSION,
      ...data,
    };
    return list;
  });
};

export const create_list = async (
  userid: DocumentID,
  listbase: ShoppingListBase,
): Promise<Document<ShoppingList>> => {
  const fixedDocumentData: Pick<ShoppingList, 'user' | 'state' | 'type' | 'items'> = {
    user: userid,
    state: ShoppingListState.IN_CREATION,
    type: ShoppingListType.SUPERMARKET,
    items: [],
  };
  const documentData: ShoppingList = {
    ...fixedDocumentData,
    ...listbase,
  };

  const documentRef = await firestore.collection(listsTable).add(documentData);
  const list: Document<ShoppingList> = {
    id: documentRef.id,
    apiVersion: SHOPPINGLIST_API_VERSION,
    ...documentData,
  };
  return list;
};

export const update_list = async (
  id: DocumentID,
  updateData: Partial<ShoppingListBase>,
): Promise<Document<ShoppingList>> => {
  const document = firestore.doc(`${listsTable}/${id}`);
  await document.update({
    ...updateData,
  });
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }

  return {
    id,
    apiVersion: SHOPPINGLIST_API_VERSION,
    ...data,
  };
};

export const update_list_items = async (id: DocumentID, itemUpdates: boolean[]) => {
  const document = firestore.doc(`${listsTable}/${id}`);
  const snapshot = await document.get();

  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }

  const newItems = data.items.map((item, idx) => {
    if (idx >= itemUpdates.length) {
      return item;
    }

    return {
      ...item,
      done: itemUpdates[idx],
    };
  });
  await document.update({
    items: newItems,
  });
};

export const delete_list = async (id: DocumentID) => firestore.doc(`${listsTable}/${id}`).delete();

export const get_list_by_id = async (listid: DocumentID): Promise<ShoppingList> => {
  const document = await firestore.doc(`${listsTable}/${listid}`).get();
  const data = document.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  return data;
};

export const get_list_document_by_id = async (
  listid: DocumentID,
): Promise<Document<ShoppingList>> => {
  const list = await get_list_by_id(listid);
  return {
    ...list,
    id: listid,
    apiVersion: SHOPPINGLIST_API_VERSION,
  };
};

export const get_open_lists_sorted_by_distance = async (
  userId: DocumentID,
): Promise<ShoppingListApi['/homies/:page?']['GET']['return']> => {
  const hero = await get_user(userId);

  if (
    hero.type !== UserType.HERO ||
    typeof hero.location === 'undefined' ||
    typeof hero.radius === 'undefined'
  ) {
    throw new InvalidDataError(`user ${userId} is not a HERO`);
  }
  const heroGeoLocation = createGeoLocation(hero.location.lat, hero.location.lng);

  const [minCoords, maxCoords] = heroGeoLocation.boundingCoordinates(hero.radius);

  // Whould be nice to query more stuff here but for some reason firestore only allows one comparison function for the location
  // So just use one, no idea which would be better than another...
  const userSnapshot = await firestore
    .collection(usersTable)
    .where('location.lat', '>=', minCoords.degreeLatitude)
    .where('type', '==', UserType.HOMIE)
    .get();

  const homiesInRange: Document<User>[] = userSnapshot.empty
    ? []
    : userSnapshot.docs
        .map((document) => {
          const data = document.data() as User | undefined;
          if (!data) {
            throw new NoDataError(`no user data for id ${document.id}`);
          }
          return {
            id: document.id,
            apiVersion: USER_API_VERSION,
            ...data,
          };
        })
        .filter((user) => {
          if (!user.location) {
            return false;
          }
          const { lat, lng } = user.location;
          return (
            lng >= minCoords.degreeLongitude &&
            lat <= maxCoords.degreeLatitude &&
            lng <= maxCoords.degreeLongitude
          );
        });

  const lists = await Promise.all(
    homiesInRange.map(async (user) => {
      const shoppinglists = await get_open_lists_by_user(user.id);
      const homieGeoLocation =
        user.location && user.location.lat && user.location.lng
          ? createGeoLocation(user.location.lat, user.location.lng)
          : null;
      const pharmacy = shoppinglists.find((list) => list.type === ShoppingListType.PHARMACY);
      const userWithShoppinglist = {
        id: user.id,
        apiVersion: USER_API_VERSION,
        displayName: user.displayName,
        photoURL: user.photoURL,
        //        shoppinglists,
        shoppinglistCount: shoppinglists.length,
        pharmacy: typeof pharmacy !== 'undefined',
        distance: truncate_number(
          homieGeoLocation && heroGeoLocation ? heroGeoLocation.distanceTo(homieGeoLocation) : -1,
        ),
      };
      return userWithShoppinglist;
    }),
  );
  const homies = lists
    .filter((user) => user.shoppinglistCount > 0)
    .sort(
      ({ distance: distanceLeft }, { distance: distanceRight }) => distanceLeft - distanceRight,
    );
  return {
    radius: truncate_number(hero.radius as number),
    hasNext: false,
    homies,
    totalHomies: homies.length,
  };
};

export const unpublish_list = async (userid: DocumentID, listid: DocumentID) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data();
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.PUBLISHED) {
    throw new InvalidActionError(`state for ${listid} isn't PUBLISHED but ${data.state}`);
  }
  if (data.user !== userid) {
    throw new InvalidActionError(`data.user for ${listid} doesn't match ${userid}`);
  }
  await document.update({
    state: ShoppingListState.IN_CREATION,
  });
};

export const take_list = async (userid: DocumentID, listid: DocumentID) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.PUBLISHED) {
    throw new InvalidActionError(`data.state for ${document.id} isn't PUBLISHED but ${data.state}`);
  }
  const [homie, hero] = await Promise.all([get_user(data.user), get_user(userid)]);

  const heroFirstname = hero.displayName.split(' ')[0];

  switch (data.type) {
    case ShoppingListType.SUPERMARKET:
      sendMessageToUser(
        homie,
        printf(gt.gettext('Ein Held hat sich gefunden')),
        printf(gt.gettext('%(heroFirstname)s kauft für dich im Supermarkt ein.'), {
          heroFirstname,
        }),
      );
      break;
    case ShoppingListType.DRUGSTORE:
      sendMessageToUser(
        homie,
        printf(gt.gettext('Ein Held hat sich gefunden')),
        printf(gt.gettext('%(heroFirstname)s kauft für dich in der Drogerie ein.'), {
          heroFirstname,
        }),
      );
      break;
    case ShoppingListType.PHARMACY:
      sendMessageToUser(
        homie,
        printf(gt.gettext('Ein Held hat sich gefunden')),
        printf(gt.gettext('%(heroFirstname)s kauft für dich in der Apotheke ein.'), {
          heroFirstname,
        }),
      );
      break;
    case ShoppingListType.MAIL:
      sendMessageToUser(
        homie,
        printf(gt.gettext('Ein Held hat sich gefunden')),
        printf(gt.gettext('%(heroFirstname)s geht für dich zur Post.'), {
          heroFirstname,
        }),
      );
      break;
    default:
      sendMessageToUser(
        homie,
        printf(gt.gettext('Ein Held hat sich gefunden')),
        printf(gt.gettext('%(heroFirstname)s kümmert sich um deine sonstigen Erledigungen'), {
          heroFirstname,
        }),
      );
  }

  await document.update({
    state: ShoppingListState.TAKEN,
    takenBy: userid,
  });
};

export const take_lists = async (userid: DocumentID, listids: DocumentID[]) => {
  Promise.all(listids.map(async (listid) => take_list(userid, listid)));
};

export const untake_list = async (userid: DocumentID, listid: DocumentID) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data();
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.TAKEN) {
    throw new InvalidActionError(`data.state for ${document.id} isn't TAKEN but ${data.state}`);
  }

  if (data.takenBy !== userid) {
    throw new InvalidActionError(`data.takenby for ${document.id} doesn't match ${userid}`);
  }

  const heroName = (await get_user(data.takenBy as string)).displayName.split(' ')[0];
  sendMessageToUserID(
    data.user,
    printf(gt.gettext('Auch ein Held ist manchmal verhindert')),
    printf(
      gt.gettext(
        'Entschuldigung! %(heroName)s hat den Einkauf leider abbrechen müssen. Mehr dazu findest du unter “Meine Einkaufszettel”',
      ),
      {
        heroName,
      },
    ),
  );

  await document.update({
    state: ShoppingListState.PUBLISHED,
    takenBy: admin.firestore.FieldValue.delete(),
  });
};

export const deliver_list = async (listid: DocumentID, userid: DocumentID, price: number) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.TAKEN || userid !== data.takenBy) {
    throw new InvalidActionError();
  }

  const newItems = data.items.map((item) => ({
    ...item,
    done: true,
  }));

  const heroName = (await get_user(data.takenBy as string)).displayName.split(' ')[0];

  sendMessageToUserID(
    data.user,
    printf(gt.gettext('Nur noch ein paar Augenblicke')),
    printf(gt.gettext('%(heroName)s ist mit deinem Einkauf auf dem Weg zu dir.'), {
      heroName,
    }),
  );
  await document.update({
    state: ShoppingListState.ON_DELIVERY,
    price,
    items: newItems,
  });
};

export const deliver_partial_list = async (
  listid: DocumentID,
  userid: DocumentID,
  price: number,
  items: boolean[],
) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.TAKEN || userid !== data.takenBy) {
    throw new InvalidActionError();
  }

  const newItemState = data.items.map((item, idx) => {
    if (idx < items.length) {
      return {
        ...item,
        done: items[idx],
      };
    }
    return item;
  });

  const anythingLeft = newItemState.filter(({ done }) => done === false).length > 0;
  const heroName = (await get_user(data.takenBy as string)).displayName.split(' ')[0];

  if (anythingLeft) {
    sendMessageToUserID(
      data.user,
      printf(gt.gettext('Fast fertig')),
      printf(
        gt.gettext(
          '%(heroName)s hat leider nicht alle Artikel bekommen und ist auf dem Weg zu dir.',
        ),
        {
          heroName,
        },
      ),
    );
  } else {
    sendMessageToUserID(
      data.user,
      printf(gt.gettext('Nur noch ein paar Augenblicke')),
      printf(gt.gettext('%(heroName)s ist mit deinem Einkauf auf dem Weg zu dir.'), {
        heroName,
      }),
    );
  }

  await document.update({
    state: ShoppingListState.ON_DELIVERY,
    price,
    items: newItemState,
  });
};

export const finish_deliver_list = async (listid: DocumentID, userid: DocumentID) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.ON_DELIVERY || userid !== data.takenBy) {
    throw new InvalidActionError();
  }
  const heroName = (await get_user(data.takenBy as string)).displayName.split(' ')[0];
  sendMessageToUserID(
    data.user,
    printf(gt.gettext('High Five!')),
    printf(gt.gettext('%(heroName)s hat den Erhalt des Geldes bestätigt.'), {
      heroName,
    }),
  );
  await document.update({
    state: ShoppingListState.DELIVERED,
  });
};

export const close_list = async (listid: DocumentID, userid: DocumentID) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }

  if (data.user !== userid || data.state !== ShoppingListState.DELIVERED) {
    throw new InvalidActionError();
  }

  const homieName = (await get_user(data.user)).displayName.split(' ')[0];
  sendMessageToUserID(
    data.takenBy as string,
    printf(gt.gettext("Drop it like it's hot")),
    printf(gt.gettext('%(homieName)s hat bestätigt, dass du den Einkauf abgegeben hast.'), {
      homieName,
    }),
  );
  await document.update({
    state: ShoppingListState.CLOSED,
  });
};

const notifiy_heroes_of_new_list = async (listid: DocumentID, userid: DocumentID) => {
  const homie = await get_user(userid);
  if (!homie.location) {
    return;
  }
  const homieGeoLocation = createGeoLocation(homie.location.lat, homie.location.lng);

  const heroSnapshots = await firestore
    .collection(`${usersTable}`)
    .where('type', '==', UserType.HERO)
    .get();

  if (heroSnapshots.empty) {
    return;
  }
  const heroesInRange: User[] = heroSnapshots.docs.reduce((acc, document) => {
    const data = document.data() as User | undefined;
    if (!data || !data.location || !data.radius || !data.notificationToken) {
      return acc;
    }
    const heroGeoLocation = createGeoLocation(data.location.lat, data.location.lng);
    const distance = heroGeoLocation.distanceTo(homieGeoLocation);

    if (distance <= data.radius) {
      logger.info(`will try to notify hero #${document.id} of new list #${listid}`);
      return [...acc, data];
    }
    return acc;
  }, [] as User[]);

  heroesInRange.forEach((hero) => {
    sendMessageToUser(
      hero,
      printf(gt.gettext('Dein Typ ist gefragt!')),
      printf(
        gt.gettext(
          'In deinem Umkreis hat jemand einen neuen Einkaufszettel angelegt. Nutze die Gelegenheit für eine gute Tat.',
        ),
      ),
    );
  });
};

export const publish_list = async (listid: DocumentID, userid: DocumentID) => {
  const document = firestore.doc(`${listsTable}/${listid}`);
  const snapshot = await document.get();
  const data = snapshot.data() as ShoppingList | undefined;
  if (!data) {
    throw new NoDataError(`list ${document.id} has no data`);
  }
  if (data.state !== ShoppingListState.IN_CREATION || data.user !== userid) {
    throw new InvalidActionError();
  }

  await document.update({
    state: ShoppingListState.PUBLISHED,
  });

  notifiy_heroes_of_new_list(listid, userid);
};

// TODO: rework this; or remove this
export const get_published_lists_for_user = async (userid: DocumentID): Promise<any> => {
  const user = await get_user(userid);
  const listSnapshot = await firestore
    .collection(listsTable)
    .where('user', '==', userid)
    .where('state', '==', ShoppingListState.PUBLISHED)
    .get();

  const shoppinglists = listSnapshot.docs.map((document) => {
    const data = document.data() as ShoppingList | undefined;
    if (!data) {
      throw new NoDataError(`list ${document.id} has no data`);
    }

    const list: Document<ShoppingList> = {
      id: document.id,
      apiVersion: SHOPPINGLIST_API_VERSION,
      ...data,
    };
    return list;
  });

  return {
    ...try_truncate_radius(user),
    id: userid,
    shoppinglists,
  };
};

export const get_lists_taken_by_user = async (
  userid: DocumentID,
): Promise<Document<ShoppingList>[]> => {
  const listSnapshot = await firestore.collection(listsTable).where('takenBy', '==', userid).get();

  const lists: Document<ShoppingList>[] = listSnapshot.docs.map((document) => {
    const data = document.data() as ShoppingList | undefined;
    if (!data) {
      throw new NoDataError(`list ${document.id} has no data`);
    }
    return { ...data, id: document.id, apiVersion: SHOPPINGLIST_API_VERSION };
  });
  return lists;
};

export const get_takenby_user_for_shoppinglist = async (
  list: Document<ShoppingList>,
): Promise<ShoppingListWithUser | Document<ShoppingList>> => {
  if (!list.takenBy) {
    return list;
  }
  const { displayName, photoURL, phone, address } = await get_user(list.takenBy);
  return {
    ...list,
    displayName,
    photoURL,
    phone,
    address,
  };
};

export const get_takenby_user_for_shoppinglists = async (
  lists: Document<ShoppingList>[],
): Promise<ShoppingListWithUser[] | Document<ShoppingList>[]> => {
  return Promise.all(lists.map(async (list) => get_takenby_user_for_shoppinglist(list)));
};

export const get_user_for_shoppinglist = async (
  list: Document<ShoppingList>,
): Promise<ShoppingListWithUser> => {
  const { displayName, photoURL, phone, address } = await get_user(list.user);
  return {
    ...list,
    displayName,
    photoURL,
    phone,
    address,
  };
};

export const get_user_for_shoppinglists = async (
  lists: Document<ShoppingList>[],
): Promise<ShoppingListWithUser[]> => {
  return Promise.all(lists.map(async (list) => get_user_for_shoppinglist(list)));
};

// const ONE_DAY_IN_MS = 86400000;

export const handle_due_lists = async (cutoff: number) => {
  const listSnapshots = await firestore.collection(listsTable).where('dueDate', '<', cutoff).get();
  if (listSnapshots.empty) {
    return;
  }
  const listBatch = firestore.batch();

  await Promise.all(
    listSnapshots.docs.map(async (doc) => {
      const list = doc.data() as ShoppingList | undefined;
      if (!list) {
        return;
      }
      switch (list.state) {
        case ShoppingListState.TAKEN:
        case ShoppingListState.ON_DELIVERY:
        case ShoppingListState.DELIVERED: {
          sendMessageToUserID(
            list.user,
            printf(gt.gettext('Leider hatte kein Held Zeit')),
            printf(
              gt.gettext(
                'Deine Liste %(listname)s ist abgelaufen und wir haben sie auf "unveröffentlicht gestellt"',
              ),
              {
                listname: list.type,
              },
            ),
          );
          if (list.takenBy) {
            const homieFirstname = (await get_user(list.user)).displayName.split(' ')[0];
            sendMessageToUserID(
              list.takenBy,
              printf(gt.gettext('Zu langsam')),
              printf(
                gt.gettext(
                  'Dein Auftrag %(listname)s von %(homieFirstname)s wurde leider nicht rechtzeitig besorgt. Wir haben ihn aus deinen Aufträgen entfernt',
                ),
                {
                  listname: list.type,
                  homieFirstname,
                },
              ),
            );
          }

          listBatch.update(doc.ref, {
            state: ShoppingListState.IN_CREATION,
            dueDate: admin.firestore.FieldValue.delete(),
          });
          break;
        }
        case ShoppingListState.PUBLISHED: {
          sendMessageToUserID(
            list.user,
            printf(gt.gettext('Leider hatte kein Held Zeit')),
            printf(
              gt.gettext(
                'Deine Liste %(listname)s ist abgelaufen und wir haben sie auf "unveröffentlicht gestellt"',
              ),
              {
                listname: list.type,
              },
            ),
          );

          listBatch.update(doc.ref, {
            state: ShoppingListState.IN_CREATION,
            dueDate: admin.firestore.FieldValue.delete(),
          });
          break;
        }
        default:
          listBatch.update(doc.ref, {
            dueDate: admin.firestore.FieldValue.delete(),
          });
          break;
      }
    }),
  );

  await listBatch.commit();
};

export const fix_wrong_lists = async () => {
  const snapshot = await firestore.collection(listsTable).get();

  const listBatch = firestore.batch();

  await Promise.all(
    snapshot.docs.map(async (doc) => {
      const list = doc.data() as ShoppingList | undefined;
      if (!list) {
        return;
      }

      const { user, takenBy } = list;
      const userSnapshot = await firestore.doc(`${usersTable}/${user}`).get();

      if (!userSnapshot.exists) {
        logger.info(
          `shopplinglist #${doc.id} belongs to user ${user} but that user doesn't exist => removing the list`,
          {
            list: doc.id,
            user,
            takenBy,
          },
        );
        listBatch.delete(doc.ref);
        return;
      }
      if (takenBy) {
        const takenBySnapshot = await firestore.doc(`${usersTable}/${takenBy}`).get();

        if (!takenBySnapshot.exists) {
          logger.info(
            `shoppinglist #${doc.id} is taken by user ${takenBy} but that user doesn't exist`,
            {
              list: doc.id,
              user,
              takenBy,
            },
          );
          switch (list.state) {
            case ShoppingListState.TAKEN:
            case ShoppingListState.ON_DELIVERY:
            case ShoppingListState.DELIVERED: {
              listBatch.update(doc.ref, {
                state: ShoppingListState.IN_CREATION,
                takenBy: admin.firestore.FieldValue.delete(),
              });
              break;
            }
            default:
              listBatch.update(doc.ref, {
                takenBy: admin.firestore.FieldValue.delete(),
              });
              break;
          }
        }
      }
    }),
  );
  await listBatch.commit();
};
