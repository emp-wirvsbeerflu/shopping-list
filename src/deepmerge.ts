import deepmergeBase from 'deepmerge';

const deepmerge = <T>(left: T, right: T | Partial<T>): T =>
  deepmergeBase<T>(left, right as Partial<T>, {
    arrayMerge: <U>(_destination: U[], source: U[]) => source,
  });

export default deepmerge;
