module.exports = {
  extends: ['airbnb-typescript/base', 'prettier'],
  plugins: ['prettier'],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    'no-console': 'off',
    '@typescript-eslint/camelcase': 'off',
    '@typescript-eslint/indent': ['off'], // let prettier indent the code,
    '@typescript-eslint/naming-convention': 'off',
  },
};
