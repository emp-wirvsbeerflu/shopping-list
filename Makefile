##
# Project Title
#
# @file
# @version 0.1



# end

SHELL := /bin/bash

PHONY+=all
all: dev

%.js:
	yarn tsc

build/db/dump.js: src/db/dump.ts
build/db/migrations/index.js: src/db/migrations/index.ts

PHONY+=dev
dev: .secrets
	source ./.secrets && yarn dev

PHONY+=database
database: .secrets
	firebase emulators:start

PHONY+=dump
dump: build/db/dump.js
	source ./.secrets && node build/db/dump.js

PHONY+=migrate
migrate: .secrets build/db/migrations/index.js
	source ./.secrets && node build/db/migrations/index.js

PHONY+=happy
happy:
	yarn messages
	yarn format
	yarn lint --fix

.PHONY: $(PHONY)
