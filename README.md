# shopping-list

## How to dev setup??

- Firebase CLI von
  https://firebase.google.com/docs/emulator-suite/install_and_configure
  installieren
- Java installieren, muss leider sein
- `firebase login` ausführen
- `firebase init` ausführen
- Da den Punkt `Emulators` auswählen, mit Leertaste markieren und dann Enter
- Mindestens `Firestore` und `Database` auswählen
- aus den Gitlab CI Variablen den `FIREBASE_KEY` ziehen und in einer Datei
  abspeichern (z.B. `firebase.json`, die ist schon im .gitignore)
- aus den Gitlab CI Variablen den `GOOGLE_SERVICEACCOUNT` ziehen und in einer Datei
  abspeichern (z.B. `gauth.json`, die ist schon im .gitignore)
- Eine Datei `.secrets` anlegen und darin dann ungefähr das rein:

```sh
#!/bin/sh
export GOOGLE_APPLICATION_CREDENTIALS="./gauth.json"
export FIREBASE_CREDENTIALS_FILE="./firebase.json"
export FIRESTORE_EMULATOR_HOST="localhost:8080"
export FIREBASE_DATABASE_EMULATOR_HOST="localhost:9000"
export ENVIRONMENT="development"
```

Pfade ggf. ersetzen

- `make database` startet den firebase Emulator
- `make dev` startet den Server

**Im dev setup erwartet der Server im `authorization` header einfach nur eine UserID**

## API Doc (very outdated)

### User

#### GET `/user/:id`

liefert die Daten zum User mit der entsprechenden id

http://shopping.evening-fjord.de/user/12345

#### Response

[User](./src/user.ts)

#### POST `/user`

Erzeugt einen User

##### Request

[UserCreatePostData](./src/user.ts)

##### Response

[User](./src/user.ts)

#### POST `/user/:id`

Updated einen User mit entsprechenden Daten

##### Request

[UserUpdatePostData](./src/user.ts)

##### Response

[User](./src/user.ts)

### Shoppinglist

#### POST `/shoppinglist`

Erzeugt eine Einkaufsliste

#### Request

[ShoppingListCreatePostData](./src/shoppinglist.ts)

##### Response

[ShoppingList](./src/shoppinglist.ts)

#### GET `/shoppinglist/:id`

Liefert die Daten zur Shoppinglist mit der entsprechenden ID

http://shopping.evening-fjord.de/shoppinglist/1

##### Response

[ShoppingList](./src/shoppinglist.ts)

#### GET `/user/:userid/shoppinglists`

Liefert alle shoppinglists zu einem User zurück

http://shopping.evening-fjord.de/user/12345/shoppinglists

##### Response

[ShoppingList](./src/shoppinglist.ts) Array

#### GET `/shoppinglists/open`

Liefert alle aktuell offenen Einkaufslisten

http://shopping.evening-fjord.de/shoppinglists/open

##### Response

[UserWithShoppinglists](./src/userwithshoppinglists.ts) Array

#### POST `/shoppinglist/:id/take`

Markiert eine Einkaufsliste als "TAKEN"

##### Request

[ShoppingListTakePostData](./src/shoppinglist.ts)

##### Response

[ShoppingList](./src/shoppinglist.ts)

#### POST `/shoppinglist/:id/deliver`

Markiert eine Einkaufsliste als Geliefert/Fertig

##### Request

[ShoppingListDeliverPostData](./src/shoppinglist.ts)

##### Response

[ShoppingList](./src/shoppinglist.ts)

#### POST `/shoppinglist/:id/publish`

Markiert eine Einkaufsliste als Veröffentlicht

##### Request

[ShoppingListPublishPostData](./src/shoppinglist.ts)

##### Response

[ShoppingList](./src/shoppinglist.ts)

#### POST `/shoppinglist/:id/add`

Fügt ein Item zur Einkaufsliste hinzu

##### Request

[ShoppingListAddItemPostData](./src/shoppinglist.ts)

#### POST `/shoppinglist/:id/remove`

Löscht ein Item von der Einkaufsliste

##### Request

[ShoppingListAddItemPostData](./src/shoppinglist.ts)

#### POST `/shoppinglist/:id/replace`

Ersetzt ein Item in der Einkaufsliste mit einem Neuen.

##### Request

[ShoppingListReplaceItemPostData](./src/shoppinglist.ts)

#### POST `/shoppinglist/:id/type`

Setzt den Typen der Einkaufsliste

##### Request

[ShoppingListChangeTypePostData](./src/shoppinglist.ts)

### Misc

#### GET `/listtypes`

Eine Liste aller möglichen Listenarten,

http://shopping.evening-fjord.de/listtypes

##### Response

[ShoppingListType](./src/shoppinglist.ts)
